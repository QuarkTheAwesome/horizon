/*
 * networkinterface.hh - interface of NetworkInterface class
 * libhorizon, common frontend routines for
 * Project Horizon
 * Wilcox Technologies, LLC
 *
 * Copyright (c) 2016 Wilcox Technologies, LLC. All rights reserved.
 * License: NCSA
 */

#ifndef NETWORKINTERFACE_HH
#define NETWORKINTERFACE_HH

#include <cstdint>
#include <string>
#include <vector>
#include <functional>
#include <net/if.h>             /* struct ifreq */
#include <netinet/in.h>         /* in[6]_addr */

#include <horizon/ssid.hh>

namespace Horizon {

//! Represents the type of a network interface.

enum NetworkInterfaceType {
	NIC_ETHERNET, //! Ethernet interface.
	NIC_WIRELESS, //! Wireless (802.11) interface.
	NIC_BONDED, //! Bonded interface.
	NIC_PPPOE, //! PPPoE interface.
	NIC_UNKNOWN = -1 //! Unknown interface type.
};

//! Callback function type for completion of long-running tasks.
typedef std::function<void (bool succeeded, std::string log_path,
        void *privdata)> NetCallbackFn;

//! Represents a network interface on the running system.

class NetworkInterface {
public:
	// Static methods
	static std::vector<NetworkInterface> list_available_ifs();
	static int send_request(std::string name, ifreq *ifreq, int type,
				std::string desc);

	// Property accessors
	std::string device_name();
	in_addr ip_address();
	in6_addr ip6_address();
	in_addr ip_subnet();
	in6_addr ip6_subnet();
	bool is_connected();
	bool is_wireless();
	NetworkInterfaceType type();

	// Common methods
	void set_ip(in_addr address, in_addr subnet, in_addr gateway = {});
	void set_ip6(in6_addr address, in6_addr subnet,
		in6_addr gateway = {});
	void try_dhcp(int timeout_sec, NetCallbackFn callback);

	// Wireless methods
	void associate(NetCallbackFn callback, std::string ssid,
		std::string passphrase = nullptr);
	std::vector<SsidInfo> list_available_ssids();
	void scan();
private:
	// ctor
	NetworkInterface(std::string name, NetworkInterfaceType if_type) :
	name(name), if_type(if_type) {
	}

	// Private data
	std::string name;
	NetworkInterfaceType if_type;
};
}

#endif // !NETWORKINTERFACE_HH
