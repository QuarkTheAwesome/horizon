/*
 * wirelesscontroller.hh - interface of WirelessController class
 * libhorizon, common frontend routines for
 * Project Horizon
 * Wilcox Technologies, LLC
 *
 * Copyright (c) 2017 Wilcox Technologies, LLC. All rights reserved.
 * License: NCSA
 */

#ifndef WIRELESSCONTROLLER_HH
#define WIRELESSCONTROLLER_HH

#include <memory>
#include <string>
#include <vector>
#include <horizon/ssid.hh>

namespace Horizon {
//! Represents the status of a wireless interface.

enum WirelessStatus {
	//! The interface is not connected to any access point.
	WIRELESS_DISCONNECTED,
	//! The interface is in the process of associating with an AP.
	WIRELESS_ASSOCIATING,
	//! The interface is connected and associated to an access point.
	WIRELESS_ASSOCIATED,
	//! The interface is in an error state.
	WIRELESS_ERROR,
	//! The interface is in an unknown state.
	WIRELESS_UNKNOWN = -1
};

/*!
 * \brief The WirelessController class is a singleton that communicates
 *        with the system to control one or more wireless interface(s).
 */
class WirelessController {
public:
	/*!
	 * \brief Retrieve the WirelessController instance, creating it if
	 *        it does not yet exist.
	 */
	static WirelessController *wirelessController();

	/*!
	 * \brief Determine if the system wireless controller is running.
	 * \return true if the system controller is running; otherwise, false.
	 */
	bool isRunning();

	/*!
	 * \brief Find information about a given (E)SSID.
	 * \param iface The wireless interface to use (wlan0, wlp3s0, ...).
	 * \param ssid  The (E)SSID to query for information.
	 * \return An SsidInfo object.  If the (E)SSID specified cannot be
	 * located, the SsidInfo will have an empty 'ssid' and all fields 0.
	 */
	SsidInfo infoForSsid(std::string iface, std::string ssid);

	/*!
	 * \brief Determine available access points on a given interface.
	 * \param iface The wireless interface to query (wlan0, wlp3s0, ...).
	 * \return The list of available SSIDs.  The list may be empty if
	 * any of the following conditions are true:
	 * * The interface does not exist or is not a wireless interface.
	 * * No networks are in range.
	 * * The controller is not running.
	 */
	std::vector<SsidInfo> availableSsids(std::string iface);

	/*!
	 * \brief Scan for access points on a given interface.
	 * \param iface The wireless interface to scan on.  You may omit this
	 * parameter to scan on all available interfaces.
	 * \note This initiates a scan, but does not return SSIDs as such a
	 * method would block.  You may poll WirelessController::availableSsids
	 * after initiating a scan, though typical drivers finish within
	 * five seconds.
	 */
	void performScan(std::string iface = nullptr);

	~WirelessController();

private:
	//! Internal implementation data class
	struct privdata;
	//! Internal implementation data holder
	std::unique_ptr<privdata> pData;

	explicit WirelessController();
};
}


#endif /* !WIRELESSCONTROLLER_HH */
