/*
 * wirelessdummy.cc - implementation of WirelessController class (no backend)
 * libhorizon, common frontend routines for
 * Project Horizon
 * Wilcox Technologies, LLC
 *
 * Copyright (c) 2017 Wilcox Technologies, LLC. All rights reserved.
 * License: NCSA
 */

#include <horizon/wirelesscontroller.hh>

using Horizon::SsidInfo;
using Horizon::WirelessController;

struct WirelessController::privdata {
};

WirelessController::WirelessController() {
	/* nothing */
}

bool WirelessController::isRunning() {
	return false;
}

SsidInfo WirelessController::infoForSsid(std::string iface, std::string ssid) {
	(void) iface;
	(void) ssid;
	return SsidInfo();
}

std::vector<SsidInfo> WirelessController::availableSsids(std::string iface) {
	(void) iface;
	return std::vector<SsidInfo>();
}

void WirelessController::performScan(std::string iface) {
	(void) iface;
}

WirelessController::~WirelessController() {
}
