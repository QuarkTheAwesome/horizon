/*
 * networkinterface.cc - implementation of NetworkInterface class
 * libhorizon, common frontend routines for
 * Project Horizon
 * Wilcox Technologies, LLC
 *
 * Copyright (c) 2016 Wilcox Technologies, LLC. All rights reserved.
 * License: NCSA
 */

#include <cassert>              /* assert */
#include <cstring>              /* strstr */
#include <iostream>             /* cerr, endl */
#include <libudev.h>            /* udev */
#include <sys/ioctl.h>          /* ioctl */
#include <sys/socket.h>         /* socket */
#include <unistd.h>             /* close */

#include <horizon/networkinterface.hh>  /* us */

using std::cerr;
using std::endl;
using std::string;
using std::vector;

using Horizon::NetworkInterface;

class TestingSocket {
public:

	TestingSocket(int type = AF_INET) {
		this->fd = ::socket(type, SOCK_DGRAM, 0);
	}

	TestingSocket(const TestingSocket &other) : fd(::dup(other.fd)) {
	}

	TestingSocket(TestingSocket &&other) : fd(::dup(other.fd)) {
		::close(other.fd);
	}

	TestingSocket& operator=(const TestingSocket &other) {
		TestingSocket sock(other);
		*this = std::move(sock);
		return *this;
	}

	TestingSocket& operator=(TestingSocket &&other) {
		::close(this->fd);
		this->fd = ::dup(other.fd);
		::close(other.fd);
		return *this;
	}

	int sockfd() {
		return this->fd;
	}

	~TestingSocket() {
		::close(this->fd);
	}
private:
	int fd;
};

int NetworkInterface::send_request(std::string name, struct ifreq *ifreq,
	int type, std::string desc) {
	TestingSocket sock;

	strncpy(ifreq->ifr_name, name.c_str(), IFNAMSIZ);
	if (ioctl(sock.sockfd(), type, ifreq) != 0) {
		cerr << "Error while " << desc << " for " << name << ": "
			<< strerror(errno) << endl;
		return errno;
	}

	return 0;
}

vector<NetworkInterface> NetworkInterface::list_available_ifs() {
	struct udev *udev;
	struct udev_enumerate *if_list;
	struct udev_list_entry *first, *candidate;
	struct udev_device *device = nullptr;

	vector<NetworkInterface> interfaces;

	udev = udev_new();
	if (udev == nullptr) {
		cerr << "could not connect to udev" << endl;
		return interfaces;
	}

	if_list = udev_enumerate_new(udev);
	if (if_list == nullptr) {
		cerr << "could not create enumerator" << endl;
		udev_unref(udev);
		return interfaces;
	}

	udev_enumerate_add_match_subsystem(if_list, "net");
	udev_enumerate_scan_devices(if_list);
	first = udev_enumerate_get_list_entry(if_list);

	udev_list_entry_foreach(candidate, first) {
		const char *syspath = udev_list_entry_get_name(candidate);
		const char *devtype, *cifname;

		if (device != nullptr) {
			udev_device_unref(device);
		}

		device = udev_device_new_from_syspath(udev, syspath);
		devtype = udev_device_get_devtype(device);
		if (devtype == nullptr) {
			/* We have no DEVTYPE, so ask the kernel */
			devtype = udev_device_get_sysattr_value(device,
				"type");
			assert(devtype != nullptr);
			if (devtype == nullptr) {
				cerr << syspath << " has no type!" << endl;
				continue;
			}
		}

		cifname = udev_device_get_property_value(device,
			"INTERFACE");
		if (cifname == nullptr) {
			cerr << syspath << " has no interface name" << endl;
			continue;
		}

		std::string ifname(cifname);
		if (ifname.length() >= IFNAMSIZ) {
			cerr << syspath << " name " << ifname <<
				" is too long for the kernel" << endl;
			continue;
		}

		Horizon::NetworkInterfaceType type;
		if (strstr(devtype, "bond")) {
			type = NIC_BONDED;
		} else if (strstr(devtype, "wlan")) {
			type = NIC_WIRELESS;
		} else if (strstr(syspath, "/virtual/")) {
			/* we don't care about things like lo or tuntap */
			continue;
		} else if (strstr(devtype, "1")) {
			type = NIC_ETHERNET;
		} else {
			type = NIC_UNKNOWN;
		}

		interfaces.push_back(NetworkInterface(ifname, type));
	}

	if (device != nullptr) {
		udev_device_unref(device);
	}

	udev_enumerate_unref(if_list);
	udev_unref(udev);
	return interfaces;
}

std::string NetworkInterface::device_name() {
	return this->name;
}

in_addr NetworkInterface::ip_address() {
	struct ifreq req = {};
	struct sockaddr_in *addr;

	if (this->send_request(this->name, &req, SIOCGIFADDR,
		"determining IPv4 address") != 0) {
		return in_addr();
	}

	addr = reinterpret_cast<struct sockaddr_in *> (&req.ifr_addr);

	return in_addr(addr->sin_addr);
}

in6_addr NetworkInterface::ip6_address() {
	return in6_addr();
}

in_addr NetworkInterface::ip_subnet() {
	struct ifreq req = {};
	struct sockaddr_in *addr;

	if (this->send_request(this->name, &req, SIOCGIFNETMASK,
		"determining IPv4 netmask") != 0) {
		return in_addr();
	}

	addr = reinterpret_cast<struct sockaddr_in *> (&req.ifr_netmask);

	return in_addr(addr->sin_addr);
}

in6_addr NetworkInterface::ip6_subnet() {
	return in6_addr();
}

bool NetworkInterface::is_connected() {
	struct ifreq req = {};

	if (this->send_request(this->name, &req, SIOCGIFFLAGS,
		"determining state") != 0) {
		return false;
	}

	return (req.ifr_flags & (IFF_UP | IFF_RUNNING));
}

bool NetworkInterface::is_wireless() {
	return (this->if_type == Horizon::NetworkInterfaceType::NIC_WIRELESS);
}

Horizon::NetworkInterfaceType NetworkInterface::type() {
	return this->if_type;
}

void NetworkInterface::set_ip(in_addr address, in_addr subnet,
	in_addr gateway) {

}

void NetworkInterface::set_ip6(in6_addr address, in6_addr subnet,
	in6_addr gateway) {

}

void NetworkInterface::try_dhcp(int timeout_sec, NetCallbackFn callback) {
	if (callback) {
		callback(false, "", NULL);
	}

	/* TODO XXX not implemented */
	return;
}

void NetworkInterface::associate(NetCallbackFn callback, std::string ssid,
	std::string passphrase) {
	if (!this->is_wireless()) {
		return;
	}

	if (callback) {
		callback(false, "", NULL);
	}

	/* TODO XXX Not implemented */
}

void NetworkInterface::scan() {
	if (!this->is_wireless()) {
		cerr << "scan() on non-wireless if " << this->name << endl;
		return;
	}

	/* TODO XXX Not implemented */
}

std::vector<Horizon::SsidInfo> NetworkInterface::list_available_ssids() {
	return vector<Horizon::SsidInfo>();
}
