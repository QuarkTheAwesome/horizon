/*
 * wirelessbcnm.cc - implementation of WirelessController class (bcnm backend)
 * libhorizon, common frontend routines for
 * Project Horizon
 * Wilcox Technologies, LLC
 *
 * Copyright (c) 2017 Wilcox Technologies, LLC. All rights reserved.
 * License: NCSA
 */

#include <bcnm/wpactrl.h>
#include <errno.h>
#include <functional>
#include <horizon/wirelesscontroller.hh>
#include <iostream>
#include <QSocketNotifier>

using Horizon::SsidInfo;
using Horizon::WirelessController;
using std::function;

struct WirelessController::privdata {
	wpactrl_t ctrl = WPACTRL_ZERO;
	bool working = false;
	char *callback_msg = nullptr;
	size_t callback_size = 0;
	WirelessController *parent = nullptr;

	int handle_callback(const char *message, size_t message_len) {
		this->callback_size += message_len;
		if (this->callback_msg == nullptr) {
			this->callback_msg =
				static_cast<char *> (malloc(message_len));
		} else {
			char *old = this->callback_msg;
			this->callback_msg =
				static_cast<char *> (
				realloc(this->callback_msg,
				this->callback_size)
				);
			if (this->callback_msg == nullptr) {
				free(old);
				/* error is handled below */
			}
		}

		if (this->callback_msg == nullptr) {
			std::cerr << "could not allocate " <<
				this->callback_size <<
				"bytes of memory" << std::endl;
			errno = ENOMEM;
			return 0;
		} else {
			memcpy(this->callback_msg +
				(this->callback_size - message_len),
				message, message_len);
		}
		return 1;
	}
};

int handle_callback(wpactrl_t *ctrl, const char *message,
	size_t message_len, void *my_fn, tain_t *time) {
	auto fn = reinterpret_cast<int(*)(const char *, size_t)> (my_fn);

	return fn(message, message_len);
}

WirelessController::WirelessController() {
	memcpy(&this->pData->ctrl, &wpactrl_zero, sizeof (wpactrl_t));
	this->pData->parent = this;

	/* initialise bcnm global */
	tain_now_g();

	if (!wpactrl_start_g(&this->pData->ctrl,
		"/var/run/wpa_supplicant.sock",
		2000)) {
		std::cerr << "wpa_supplicant seems to be broken!"
			<< std::endl;
		return;
	}

	this->pData->working = true;
}

bool WirelessController::isRunning() {
	if (!this->pData->working) {
		return false;
	}

	return (wpactrl_command_g(&this->pData->ctrl, "PING") == WPA_PONG);
}

SsidInfo WirelessController::infoForSsid(std::string iface, std::string ssid) {
	return SsidInfo();
}

std::vector<SsidInfo> WirelessController::availableSsids(std::string iface) {
	(void) iface;
	return std::vector<SsidInfo>();
}

void WirelessController::performScan(std::string iface) {
	static wpactrl_xchgitem_t callback_table = {
		.filter = "CTRL-EVENT-SCAN-RESULTS",
		.cb = &handle_callback
	};
	wparesponse_t res = wpactrl_command_g(&this->pData->ctrl, "SCAN");
	tain_t deadline;
	wpactrl_xchg_t data;
	using std::placeholders::_1;
	using std::placeholders::_2;
	function<int(const char *, size_t) > my_callback =
		std::bind(&WirelessController::privdata::handle_callback,
		*this->pData, _1, _2);

	if (res != WPA_OK && res != WPA_FAILBUSY) {
		std::cerr << "could not SCAN: " << res << std::endl;
		/* we have failed. */
		return;
	}

	tain_from_millisecs(&deadline, 5000);
	tain_add_g(&deadline, &deadline);
	wpactrl_xchg_init(&data, &callback_table, 1, &deadline,
		my_callback.target<int(*)(const char *, size_t)>());
	if (!wpactrl_xchg_start(&this->pData->ctrl, &data)) {
		std::cerr << "failed to start scan: " << strerror(errno)
			<< std::endl;
		return;
	}
}

WirelessController::~WirelessController() {
	wpactrl_end(&this->pData->ctrl);
}
