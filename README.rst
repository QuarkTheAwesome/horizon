=========
 Horizon
=========
:Authors:
  * **A. Wilcox**
  * **Elizabeth Myers**
:Version:
  1.0
:Status:
  Concept
:Copyright:
  © 2015-2016 Adélie Linux Team.  NCSA open source licence.



Requirements
============

Background
----------
Our new Linux distro, Adélie, requires an installer system.  The ideal system
will be able to provide the user with flexibility while also choosing safe
defaults so that users can get up and running quickly.  It will also provide
a way to script installations, such as Anaconda's Kickstart or Debian's Preseed.


Opportunity
-----------
The current distribution landscape contains three major types of installation
routines: those that hold the user's hands at the expense of flexibility
(batteries included), those that are rigid and don't work well outside of a
limited scope (fixed), and those that require the user to do most, if not all,
of the installation themselves using a command line (manual).

Batteries included
``````````````````
* The Ubuntu distribution uses an installer named Ubiquity.  Ubiquity is very
  user friendly; however, it has many shortcomings.  It cannot be scripted -
  administrators must use the Debian installer with a preseed file to script
  an Ubuntu installation.  The partitioning options offered are limited.  It
  uses DBus and udevadm to set up networking, which is not robust.  It is
  focused on x86 and unlikely to work well on other architectures.  It uses
  Debian's partman internally for partitioning, which is a mess on its own.

* The Debian distribution uses debian-installer, or d-i.  d-i offers a lot of
  flexibility with plug-in installation steps.  However, it suffers from many
  limitations.  The scripting format, termed "preseed", is extremely convoluted
  and very poorly documented.  It has been described as a "nightmare" by its own
  users[1] and has many warts as a result of being partially written in Perl,
  and partially in C, which resulted in a maintenance burden.

  In addition, d-i does not always support common hardware, especially on
  the remastered CDs required for scripting - notably, on Debian 7, the SATA
  drivers are not added which renders it unusable on most modern computers.  It
  suffers from a severe lack of predictable results; given the same input, it
  does not always produce the same output.  This is due to various race
  conditions throughout the system.

  [1]: https://lists.debian.org/debian-boot/2012/01/msg00016.html

* The Red Hat and Fedora family of distributions use the Anaconda installer.
  This is one of the oldest systems still in use today, and is wholly written in
  Python.  Scripting support via Kickstart files are much better than d-i, but
  still suffer a number of problems.  Repository signatures cannot be added, so
  if packages are required that are not present in the base RHEL/Fedora repo,
  you must allow unsigned packages to be installed.  This can present a security
  risk in the case of DNS or ARP poisoning.

  Further, until the most recent version of Fedora, the only manner supported of
  wireless network authentication was WEP, which is highly insecure.  The newest
  version additionally supports WPA passphrases.  It does not support RADIUS
  authentication or certificate-based authentication.

Fixed
`````
* The Alpine Linux installer is a simple set of shell scripts that offers very
  few options.  A user can choose between using a hard disk for the entire
  system or for data only (leaving the OS and software in a tmpfs).  There is
  no option to use an existing partition map, or to use an alternative to LVM2.
  There is also no support for RAID.  As it is a set of interactive shell
  scripts, it does not make sense to script an installation using it; typically,
  users script a manual installation (such as in the Manual section).

  There is also no support for using IPv6 in the Alpine Linux installer, which
  is crucial for the modern Internet.

* FreeBSD and OpenBSD, while not strictly Linux distributions, also fall in this
  category.  OpenBSD's installation system is very close to Alpine's, while the
  FreeBSD bsdinstall makes many assumptions about the user's desired choices and
  offers very little customisation.  One area where bsdinstall succeeds is the
  partition interface.

  None of the three major BSD derivatives support any form of scripted install.

Manual
``````
* The ArchLinux distribution provides a list of commands for the user to run,
  and provides a very minimal environment on their boot CD to allow them to run.
  You must manually bootstrap the package manager, Pacman, and install the base
  system packages yourself.  This lends itself to scripting, but it again must
  be entirely manual.  The user would be scripting the disk manipulation, clock
  set up, package database initialisation, and so on, themselves.

* The Gentoo distribution provides a "stage" tarball containing a base system.
  The user must create partitions manually before extracting the stage to the
  target.  The user must also build and install their own kernel and bootloader.
  As with ArchLinux, any scripting would be automating the low-level commands
  themselves; there is no provided framework for automating installs in Gentoo.


Objectives / success criteria
-----------------------------
#. Users new to Adélie able to install for the first time in under 15 minutes.

#. Positive reviews in at least one publication (such as Linux Journal).

#. 100% compatibility with all Tier 1 platforms.




Solution vision
===============

Vision statement
----------------
For computer users who wish to use Adélie Linux, Project Horizon is an
installation system that is easy to use and highly scriptable.  Unlike other
Linux installers, Project Horizon will be maintainable, usable, and flexible.


Major features
--------------
#. Distinct installation phases.

   #. The ability to add and remove phases as deemed necessary.

   #. Profile selection - libc, init system.

   #. Disk partitioning.

   #. Mounting disk partitions.

   #. Network configuration.

   #. Installing base system from profile.

   #. Additional profile and set selection - desktop environment, server type.

   #. Installing additional packages.

   #. Configuration of installed packages.

   #. Architecture-specific phases.


#. Scriptability of each phase:

   #. Ability to run a script before a phase, allowing phase-specific 'hints'
      to be provided by the script.

   #. Ability to run a script after a phase.  For example, adding additional
      packages to the base system.


Assumptions
-----------
#. We assume that any scripts provided are safe.  They are executed in a
   privileged context.

#. We assume that Project Horizon will not be run on a system that has already
   been configured.  This may change at a later date, at least for set selection
   and profile selection.


Dependencies
------------
#. The system will require a disk partitioning system that can be manipulated
   via Python.

#. The system shall comply with FHS 3.0 as specified by the Linux Foundation.[1]

[1]: http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf




Project Scope and Limitations
=============================

Scope of initial release (v1)
-----------------------------
Emphasis must be focused on maintainability and flexibility during initial
development work.  The most important phases that need to be reliability are the
disk partitioning phase, which must not cause unintended data loss, and the
configuration of packages phase.

All phases must work, to some degree, but it is understood that the initial
release may not be entirely polished in some areas.

Scripts made for v1 do not have a guarantee of forward compatibility with later
releases.  This allows a degree of freedom in experimenting with different ways
to implement scripts so that the ideal solution can be architected.


Scope of second release (v2)
----------------------------
We envision that the profile and set selection phases will be refined the most
in the second release.  Networking configuration is another important phase that
should receive more polish if necessary.

Scripts made for any release after v1 must remain compatible with future release
for at least two futher release cycles.  For example, v2 scripts are guaranteed
to run on v3 and v4.


Scope of future releases (vX)
-----------------------------
A few ideas have been thrown around, and will be investigated during future
release cycles.  These may or may not be implemented:

#. Dynamic loading of phases.

#. Installation to a network device (NFS, CIFS, etc).

#. Building a custom kernel based on the user's configuration options.

#. Ability to use Horizon technologies on systems already installed (switching
   profiles from musl to glibc, for instance).


Limitations
-----------
#. Project Horizon will not function as an installer for other distributions,
   including any other APK distributions.  This allows us a degree of freedom
   with regards to technologies used.




Non-functional requirements
===========================

This section details the non-functional requirements for Project Horizon as
identified when this document was last modified.


Integrity
---------
#. All packages installed using Horizon must be signed by a key trusted by APK.

#. All Internet traffic conducted by Horizon must be performed using TLS v1.2
   or later.


Robustness
----------
#. If the system runs out of disk space during any phase, ????

#. If there is an otherwise-unspecified disk I/O error during any phase, ????

#. If the system encounters a network error during any phase, ????

#. If the system encounters an error during the package configuration phase, ??

#. If a script encounters an error, ????  separate pre/post requirements?
